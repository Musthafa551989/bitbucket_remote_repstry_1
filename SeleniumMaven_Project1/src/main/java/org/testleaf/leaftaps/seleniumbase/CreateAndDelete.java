package org.testleaf.leaftaps.seleniumbase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testleaf.leaftaps.seleniumbase.ProjectSpecificMethods;
import org.testng.annotations.*;
import org.testng.annotations.Test;

public class CreateAndDelete extends ProjectSpecificMethods {
	
	@BeforeClass
	public void setData() {
		excelFileName = "CreateLead";		// Pass Excelfile name...
	}

	@Test(dataProvider="fetchDataExcel")
	public void createAndDelete(String cName,String fName,String lName) throws InterruptedException {
		
		driver.findElementByLinkText("Create Lead").click();

		// Create Lead class
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		driver.findElementByName("submitButton").click();

		// Get the Lead Id
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		String leadId = text.replaceAll("\\D", "");
		// Delete the Lead
		driver.findElementByLinkText("Delete").click();
		// Confirm it is deleted
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys(leadId);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		Thread.sleep(5000);		
		boolean displayed = driver.findElementByXPath("//div[text()='No records to display']").isDisplayed();

		System.out.println(displayed);

	}

}

package org.testleaf.leaftaps.seleniumbase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import utils.ReadExcelFile;

public class ProjectSpecificMethods {
	
	public ChromeDriver driver;
	public String excelFileName;
	
	@Parameters({"url","uName","pwd"})
	@BeforeMethod	
	public void aLogin(String urlA,String uNameA,String pwdA)
	{
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		//launch chrome
		driver = new ChromeDriver();
		//load the URL
		driver.get(urlA);
		// implicit wait
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//maximize the Browser 
		driver.manage().window().maximize();
		//login
		driver.findElementById("username").sendKeys(uNameA);
		
		driver.findElementById("password").sendKeys(pwdA);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@AfterMethod
	public void CloseBrowser()
	{
		driver.close();
	}
	
	@BeforeSuite
	public void beforeSuite()
	{
		System.out.println("Connects to RemoteServer");
	}
	@AfterSuite
	public void AfterSuite()
	{
		System.out.println("Disconnect RemoteServer Connection");
	}
	@BeforeTest
	public void beforeTest()
	{
		System.out.println("Connects to DB");
	}
	@AfterTest
	public void AfterTest()
	{
		System.out.println("Disconnect DB");
	}
	@BeforeClass
	public void beforeClass()
	{
		System.out.println("Execute query for Class");
	}
	@AfterClass
	public void afterClass()
	{
		System.out.println("Close or Clear object connection");
	}
	
	@DataProvider(name="fetchDataExcel")
	public String[][] getData() throws IOException
	{
				
		return ReadExcelFile.readExcelFile(excelFileName);
	}

}

package org.testleaf.leaftaps.seleniumbase;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testleaf.leaftaps.seleniumbase.ProjectSpecificMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FindLeads extends ProjectSpecificMethods  {
	
	@BeforeClass
	public void setData() {
		
		excelFileName = "FIndLeads";		
	}

	@Test(enabled=false)
	public void findLeads() {
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[@class='linktext'])[1]")));
		driver.findElementByXPath("(//a[@class='linktext'])[]").click();

	}

}

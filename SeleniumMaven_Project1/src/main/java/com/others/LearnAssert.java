package com.others;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LearnAssert {
	// usage of css locators
	// driver.findElementByCssSelector("input[name='USERNAME']");

	// To handle authentication
//	driver.get("http://admin:password@10.0.0.1");

	@Test
	public void login() throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		prop.load(new FileInputStream("english.properties"));
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps");
		System.out.println(driver.getTitle());
		driver.findElement(By.id(prop.getProperty("LoginPage.username.id"))).sendKeys("DemoCSR");
		driver.findElement(By.id(prop.getProperty("LoginPage.password.id"))).sendKeys("crmsfa");
		driver.findElement(By.xpath("//input[@class='decorativeSubmit']")).click();
		String welcomeDemoSales = driver.findElement(By.xpath("//div[@id='form']//h2[1]")).getText();
		Assert.assertEquals(welcomeDemoSales, "Welcome\nDemo Sales Manager");

	}

}

package com.others;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry implements IRetryAnalyzer {

	int iMax = 0;
	
	@Override
	public boolean retry(ITestResult result) {
		if(iMax < 1) {
			iMax++;
			return true;
		}else {
			return false;
		}
	}
	

}

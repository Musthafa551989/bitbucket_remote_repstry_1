package com.others;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	@Before
	public void beforeScenario(Scenario sc) {
		System.out.println(sc.getName());
		// print scenario no
		System.out.println(sc.getId());
		// get tag names
		System.out.println(sc.getSourceTagNames());

	}

	@After
	public void afterScenario(Scenario sc) {
		// return status
		System.out.println(sc.getStatus());
		// return is failed or not
		System.out.println(sc.isFailed());
	}

}

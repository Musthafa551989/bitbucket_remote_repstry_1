package com.others;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.annotations.ITestAnnotation;

public class LearnListeners implements IRetryAnalyzer, IAnnotationTransformer {

	boolean result = false;
	public static int count = 1;
	@Override
	public boolean retry(ITestResult status) {
		if (!status.isSuccess() && count < 2) {
			result = true;
			count++;
		}
		return result;
	}
	@Override
	public void transform(ITestAnnotation arg0, Class arg1,
			Constructor arg2, Method arg3) {
		arg0.setRetryAnalyzer(LearnListeners.class);
		if (arg1.getName().contains("TC001")) {
			arg0.setEnabled(true);
		}		
	}
}

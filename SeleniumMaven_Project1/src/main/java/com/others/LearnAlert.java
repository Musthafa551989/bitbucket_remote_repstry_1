package com.others;


import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Alert.html");
		driver.manage().window().maximize();
		driver.
		findElementByXPath("//button[text()='Prompt Box']")
		.click();
		// switch to alert
		Alert alert = driver.switchTo().alert();
		// Get the text from alert box
		String text = alert.getText();
		System.out.println(text);
		// type on the alert box
		alert.sendKeys("Testleaf Nanganallur");
		// click on OK button
		alert.accept();		
		
	}

}

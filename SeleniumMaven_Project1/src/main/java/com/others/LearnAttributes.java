package com.others;

import org.testng.annotations.Test;

public class LearnAttributes {
	/* @Test(priority = -22) */
	// @Test(enabled = false)
	@Test
	public void leafTapsCL() {
		System.out.println("leafTapsCL");
		throw new RuntimeException();
	}

	// @Test(priority = 1)
	@Test(dependsOnMethods = "leafTapsCL", alwaysRun = true)
	// @Test
	public void editLead() {
		System.out.println("editLead");
	}

	// @Test(priority = 1)
	// @Test(dependsOnMethods = { "editLead", "leafTapsCL" })
	@Test
	public void deleteLead() {
		System.out.println("deleteLead");
	}
}

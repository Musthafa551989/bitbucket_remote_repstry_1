package utils;

import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ReadExcelFile {
		
	public static String[][] readExcelFile (String wBookName ) throws IOException
	{
				
		// Got to file location and open file......123456
		XSSFWorkbook wBook = new XSSFWorkbook("./data/"+wBookName+".xlsx");
		
		//Navigate to particular sheet
		
		XSSFSheet sheet = wBook.getSheetAt(0);
		
		// Find no of rows in the sheet
		
		int rowCount = sheet.getLastRowNum();
		
		//Find no of columns in th sheet
		
		int colCount = sheet.getRow(0).getLastCellNum();
		
		String[][] data = new String[rowCount][colCount];
		
		for (int i=1;i<=rowCount;i++)
		{
			XSSFRow row = sheet.getRow(i);
			
			for(int j=0;j<colCount;j++)
			{
				XSSFCell cell = row.getCell(j);
				data [i-1][j] = cell.getStringCellValue();;
				System.out.println(data [i-1][j]);
			}
		}
		
		wBook.close();
		return data;
	}

}
